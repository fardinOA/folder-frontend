import Head from "next/head";
import useSWR from "swr";
import Image from "next/image";
import styles from "../styles/Home.module.css";
import Folders from "../components/Folders";
const fetcher = (url: RequestInfo | URL) =>
    fetch(url).then((res) => res.json());
// const url = "http://localhost:2020/folder-id";
const url = "https://folder-backend.vercel.app/folder-id";
export default function Home() {
    const { data, error } = useSWR(url, fetcher);

    return (
        <div className={` h-screen w-screen `}>
            <Folders ids={data?.folders} />
        </div>
    );
}
