import React, { useState, useEffect } from "react";
import axios from "axios";

import styles from "./Folders.module.css";
import ShowFolder from "./ShowFolder";
import Loader from "../Loader";

const Folder = ({ ids }: { ids: [] }) => {
    const [folders, setFolders] = useState([]);
    const [loading, setLoading] = useState(true);
    useEffect(() => {
        const getFolder = async () => {
            const config = {
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
            };
            const { data } = await axios.post(
                "https://folder-backend.vercel.app/folders",
                {
                    ids,
                },
                config
            );

            setFolders(data.folders);
            if (data.folders) setLoading(false);
        };
        getFolder();
    }, [ids]);

    if (loading) return <Loader />;
    return (
        <div className="container p-4 m-auto">
            {folders.length > 0 ? (
                folders?.map((ele: any) => (
                    <ShowFolder key={ele._id} ele={ele} />
                ))
            ) : (
                <div className="ml-12  w-[8rem] font-bold text-red-500 flex ">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className="w-6 h-6"
                    >
                        <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M15.182 16.318A4.486 4.486 0 0012.016 15a4.486 4.486 0 00-3.198 1.318M21 12a9 9 0 11-18 0 9 9 0 0118 0zM9.75 9.75c0 .414-.168.75-.375.75S9 10.164 9 9.75 9.168 9 9.375 9s.375.336.375.75zm-.375 0h.008v.015h-.008V9.75zm5.625 0c0 .414-.168.75-.375.75s-.375-.336-.375-.75.168-.75.375-.75.375.336.375.75zm-.375 0h.008v.015h-.008V9.75z"
                        />
                    </svg>
                    No folder Yet
                </div>
            )}
        </div>
    );
};

export default Folder;
