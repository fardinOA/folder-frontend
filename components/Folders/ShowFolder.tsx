import React, { useState } from "react";
import {
    AiOutlineArrowDown,
    AiOutlineArrowUp,
    AiOutlineCloseCircle,
} from "react-icons/ai";
import Folder from "./index";
import styles from "./Folders.module.css";
import axios from "axios";
import Router from "next/router";
const ShowFolder = ({ ele }: { ele: any }) => {
    const [showSubFolder, setShowSubFolder] = useState({
        id: "",
        status: false,
    });
    const [showAddFolder, setShowAddFolder] = useState({
        id: "",
        status: false,
    });
    const [folderName, setFolderName] = useState("");

    const addHandeler = async (
        e: React.MouseEvent<HTMLButtonElement, MouseEvent>,
        id: any
    ) => {
        e.preventDefault();
        try {
            const config = {
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
            };
            const { data } = await axios.post(
                "https://folder-backend.vercel.app/add-folder",
                {
                    id,
                    folderName: folderName,
                },
                config
            );
            if (data.message) {
                setShowAddFolder({ ...showAddFolder, status: false });
                Router.reload();
            }
        } catch (error) {}
    };

    const deleteHandeler = async (id: any, name: String) => {
        if (name == "root") window.alert("You Can't Delete Root Folder");
        else {
            try {
                const config = {
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                };
                const { data } = await axios.delete(
                    `https://folder-backend.vercel.app/delete/${id}`
                );
                if (data.message) {
                    Router.reload();
                }
            } catch (error) {}
        }
    };
    return (
        <div className={`flex justify-between w-full`} key={ele._id}>
            <div>
                <div className={`flex p-4 gap-2 text-center`}>
                    <span>
                        {showSubFolder.status && showSubFolder.id == ele._id ? (
                            <AiOutlineArrowUp
                                onClick={() =>
                                    setShowSubFolder({
                                        ...showSubFolder,
                                        id: ele._id,
                                        status: !showSubFolder.status,
                                    })
                                }
                                size={30}
                                className="transition-all cursor-pointer duration-500"
                            />
                        ) : (
                            <AiOutlineArrowDown
                                size={30}
                                onClick={() =>
                                    setShowSubFolder({
                                        ...showSubFolder,
                                        id: ele._id,
                                        status: !showSubFolder.status,
                                    })
                                }
                                className="transition-all cursor-pointer duration-500"
                            />
                        )}
                    </span>
                    <span className="    text-[1rem] md:text-[1.5rem] capitalize font-black">
                        {" "}
                        {ele.folderName}{" "}
                    </span>
                    <span>
                        <AiOutlineCloseCircle
                            className="cursor-pointer "
                            size={30}
                            onClick={() =>
                                deleteHandeler(ele._id, ele.folderName)
                            }
                        />
                    </span>
                </div>
                {showSubFolder.status && showSubFolder.id == ele._id && (
                    <div>
                        <Folder ids={ele.subFolders} />
                    </div>
                )}
            </div>
            <div className=" h-[2rem] px-4 mt-4 rounded-md hover:bg-teal-600 transition-all duration-300 py-1 border">
                <button
                    onClick={() =>
                        setShowAddFolder({
                            ...showAddFolder,
                            id: ele._id,
                            status: !showAddFolder.status,
                        })
                    }
                    className="flex"
                >
                    {" "}
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className="w-6 h-6"
                    >
                        <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M12 4.5v15m7.5-7.5h-15"
                        />
                    </svg>
                    <span> New </span>
                </button>
            </div>

            {showAddFolder.status && showAddFolder.id == ele._id && (
                // <div className={styles.formContainer}>

                // </div>

                <div className="flex justify-center items-center">
                    <div
                        id="defaultModal"
                        tabIndex={-1}
                        aria-hidden="true"
                        className="   overflow-y-auto overflow-x-hidden fixed top-0 left-0 md:left-[5%] lg:left-[30%] transition-all duration-300 z-50 p-4 w-full    md:h-full"
                    >
                        <div className="relative w-full max-w-2xl h-full md:h-auto">
                            <div className="relative bg-white rounded-lg shadow dark:bg-gray-700">
                                <form>
                                    <div className="p-4">
                                        <label
                                            htmlFor="first_name"
                                            className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                                        >
                                            Folder Name
                                        </label>
                                        <input
                                            onChange={(e) =>
                                                setFolderName(e.target.value)
                                            }
                                            type="text"
                                            id="first_name"
                                            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            placeholder="New Folder "
                                            required
                                        />
                                    </div>
                                </form>
                                <div className="flex items-center justify-between p-6 space-x-2 rounded-b border-t border-gray-200 dark:border-gray-600">
                                    <button
                                        onClick={() =>
                                            setShowAddFolder({
                                                ...showAddFolder,
                                                status: false,
                                            })
                                        }
                                        data-modal-toggle="defaultModal"
                                        type="button"
                                        className="text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
                                    >
                                        Cancle
                                    </button>
                                    <button
                                        onClick={(e) => addHandeler(e, ele._id)}
                                        data-modal-toggle="defaultModal"
                                        type="button"
                                        className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600"
                                    >
                                        Add
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
};

export default ShowFolder;
