
# Hi, I'm Fardin Omor Afnan! 👋

This is a simple folder structure frontend project. Hope you like it 😍
## Run the project on your machine

Clone git repository

```bash
 git clone https://gitlab.com/fardinOA/folder-frontend
```

Install packeges

```bash
  npm install or yarn
```

Go to project folder

```bash
  cd folder-frontend
```

Run locally

```bash
  npm run dev
```
## Demo

https://folder-frontend.vercel.app/


## Support

For support, email afnan.eu.cse@gmail.com  

